package main;

public class FibonacciApp {
    private static void fibWhile(int n){
        int a = 1 , b=1, temp=0;
        if (n>=1) System.out.print(a + " ");
        if (n>=2) System.out.print(b + " ");
        while (n>=3){
            temp = b;
            b = a + b;
            a = temp;
            System.out.print(b+" ");
            n--;
        }
        System.out.println();
    }

    private static void fibFor(int n){
        int a = 1, b=1, temp;
        if (n>=1) System.out.print(a + " ");
        if (n>=2) System.out.print(b + " ");
        for(int i=3; i<=n; i++){
            temp = b;
            b = a + b;
            a = temp;
            System.out.print(b+" ");
        }
    }

    public static void main(String[] args) {
        fibWhile(11);
        fibFor(11);
    }
}
